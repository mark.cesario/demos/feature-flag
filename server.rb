require 'webrick'
require 'securerandom'
require 'unleash'
require 'unleash/context'

unleash = Unleash::Client.new({
  url: ENV.fetch('UNLEASH_URL',''),
  app_name: ENV.fetch('UNLEASH_APP',''),
  instance_id: ENV.fetch('UNLEASH_INSTANCE_ID','')
})

unleash_context = Unleash::Context.new
unleash_context.user_id = "123"

server = WEBrick::HTTPServer.new :Port => ENV.fetch('PORT'){ 5000 }

#Return a successful Hello World response.
server.mount_proc '/' do |_request, response|
  if unleash.is_enabled?("ff_demo", unleash_context)
    response.body = '<h1>I am behind feature flag in environment ' + unleash_context.app_name + " for <customer>!</h1>"
  else
    response.body = '<h1>Hello world from GitLab !</h1>'
  end
  sleep 0.125 + SecureRandom.random_number*0.250
end

#Return an error to help demonstrate monitoring capabilities
server.mount_proc '/error' do |_request, response|
  response.status = 500
  response.body = 'Sorry we encountered an error.'
  sleep 1.0+SecureRandom.random_number
end

server.start
