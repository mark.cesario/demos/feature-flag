FROM ruby:2.5.1-alpine
ADD ./ /app/
WORKDIR /app
ENV PORT 5000
EXPOSE 5000


# Install gems
RUN apk add --no-cache --virtual build-deps build-base && \
  bundle install && \
  apk del build-deps


CMD ["sh", "-c", "while :; do ruby ./server.rb; done"]
